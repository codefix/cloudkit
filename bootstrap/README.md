gCloud Initialization Commands
==============================

  1. Create an instance FOO

        gcloud compute instances create FOO \ 
        --image ubuntu-14-04 \ 
        --machine-type g1-small \ 
        --metadata startup-script-url=gs://bootstrap.cdfx.us/bootstrap-root,bootstrap-url=BOOT-FOO

    For CDFX and Skynet instances, BOOT-FOO is one of...

        gs://bootstrap.cdfx.us/bootstrap-FOO
        gs://bss.cdfx.us/bootstrap-FOO

    See all machine types and images:

        gcloud compute machine-types list
        gcloud compute images list

  2. Connect to the instance.

        gcloud compute ssh FOO --ssh-key-file=~/.ssh/id_rsa

  3. Bootstrap.

        /tmp/bootstrap.sh

Notes
-----

  * User accounts are not automatically created, but all can log in
    via web console
    ([codefix](https://console.cloud.google.com/compute/instances?project=codefix-1223),
    [skynet](https://console.cloud.google.com/compute/instances?project=skynet-1226))
    or step 2 (above).
    See [cloud.google.com/sdk/](https://cloud.google.com/sdk/)
