#!/usr/bin/zsh
typeset -a dbNames dbUsers
typeset dbUser dbPass dbSkips dbGrants bzPath bzPrefix bzExpire
typeset VERBOSE=0 cmdPurge
die () { #{{{
    [[ ($VERBOSE -ge 0) ]] && print -P "%F{red}" $@ '%f';
    exit 10;
} #}}}
sayif () { #{{{
    local V=$1; shift
    [[ ($VERBOSE < $V) ]] && return
    if [[ $V == 1 ]] {
        print -P '%F{blue}%B' $@ '%b%f';
    } else {
        print -P '%F{cyan}' $@ '%f';
    }
} #}}}
usage() { #{{{
    { # default to $PAGER if pandoc isn't in $PATH.
        if [[ -z $(whence pandoc) ]] {
            : ${PAGER:=less}
            ${PAGER}
        } else {
            pandoc --normalize -RSst man | man -l -
        }
    } <<-\EOF
	% MYSQL-BACKUP(1) MySQL Backup user manual
	% Gary Hoffman <garrison.hoffman@gmail.com>
	
	<!--
	        Install pandoc to view this as a man page.
	-->
	
	# NAME
	
	mysql-backup --- MySQL Backup tool
	
	# SYNOPSIS
	
	mysql-backup [*OPTIONS*] [*DATABASES*]
	
	# DESCRIPTION
	
	Intended as a tool to facilitate MySQL backups. If no databases are
	specified, all databases will be archived excepting those specified
	by **dbSkips**. Archives are compressed with bzip2 and checksums created
	via SHA1 message digest (sha1sum).
	
	# OPTIONS
	
	-c, \--conf=*CONFIG*
	:   Specify a config file. (default: **~/.config/mysql-backup.cf**)
	
	-e, \--expire=*N*
	:   Purge archive files after *N* days. Values of zero or lower disable.
	    The **trash** command will be used where availible, otherwise **rm**.
	
	-g, \--grants
	:   Archive MySQL grants to **\${bzPath}/\${bzPrefix}-GRANTS.sql.bz2**.
	
	-h, \--help
	:   Show usage, then exit.
	
	-u, \--user=*USER*
	:   Access MySQL as *USER*. (default: **id -un**)
	
	-v, -q
	:   Increment, decrement verbosity with each use.
	    (effective range -q through -vv)
	
	\--verbosity=*N*
	:   Set an explicit verbosity level.
	    Initial (default) verbosity is *0*; values lower than zero will
	    silence even fatal warnings.
	
	# CONFIG FILE
	
	Default location is **~/.config/mysql-backup.cf**. The only required
	variable is **dbPass**.
	
	dbUser='*Username*'
	:   MySQL username. (default: **id -un**)
	
	dbPass='*Password*'
	:   MySQL password. (required)
	
	dbUsers=*('user1' 'user2'..'userN')*
	:   Users to include when archiving grants.
	    (default: all but **root** and **debian-sys-maint**)
	
	dbNames=*('database1' 'database2'..'databaseN')*
	:   Databases to archive when none are supplied on the command line.
	    (default: all but those specified by **dbSkips**)
	
	dbSkips='*database1|database2..databaseN*'
	:   Databases to skip when none are specified on the command line.
	    Irrelevant when database names are supplied on the command line or
	    via **dbNames**.
	    (default: **mysql|information_schema|performance_schema**)
	
	bzPath=*Path*
	:   Backup file path. (default: **~/mysql-backups**)
	
	bzPrefix=*Prefix*
	:   Backup file prefix, defaults to **YYYY-MM-DD-${HOST}**.
	
	bzExpire=*N*
	:   Purge archive files after *N* days. Disabled (0) by default.
	    The **trash** command will be used where availible, otherwise **rm**.
	EOF
    exit
} #}}}
bkGrants () { # {{{
    typeset sqlGrants SQL
    SQL="SELECT CONCAT('\'',user,'\'@\'',host,'\'') FROM mysql.user"
    if [[ -z ${dbUsers} ]] {
        SQL+=" WHERE user IS NOT NULL"
        SQL+="  AND user != 'root' AND user != 'debian-sys-maint';"
    } else {
        SQL+=" WHERE FALSE"
        for U (${dbUsers[@]}) {
            SQL+=" OR user = '${U}'"
        }
        SQL+=" ORDER BY user;"
    }
    dbUsers=($(mysql -u${dbUser} -p${dbPass} -Ne${SQL}))
    [[ -z ${dbUsers} ]] && die 'User query failed.'
    sqlGrants="${bzPath}/${bzPrefix}-GRANTS.sql"

    truncate --size 0 ${sqlGrants} && chmod 600 ${sqlGrants}
    sayif 1 "Saving MySQL grants…"
    for U (${dbUsers[@]}) {
        sayif 2 "  Writing grants for %F{magenta}${U:s/%/%%/}"
        SQL="show grants for ${U};"
        mysql -u${dbUser} -p${dbPass} -BNe${SQL} | sed 's/$/;/; s/\\\\/\\/g' \
            >> ${sqlGrants}
    }
    sayif 2 "  Writing %F{blue}${sqlGrants:t}.bz2"
    bzip2 --compress --force -9 ${sqlGrants}
    sayif 2 "  Writing %F{blue}${sqlGrants:t}.bz2.sha1"
    sha1sum "${sqlGrants}.bz2" > "${sqlGrants}.bz2.sha1"
} # }}}
bkDatabases () { # {{{
    typeset bzFile
    if [[ -z ${dbNames} ]] {
        dbNames=($(mysql -u${dbUser} -p${dbPass} -Ne'SHOW DATABASES;'))
    }
    for db (${dbNames[@]}) {
        [[ ${db} =~ ${dbSkips} ]] && continue
        mysql -u${dbUser} -p${dbPass} -e"use ${db}" 2> /dev/null
        if [[ $? != 0 ]] {
            sayif 0 "%F{y}Cannot access%f %F{r}${db}"
            continue
        }
        bzFile="${bzPath}/${bzPrefix}-${db}.sql.bz2"
        sayif 1 "Backing up %F{cyan}${db}%F{blue}…"
        sayif 2 "  Writing %F{blue}${bzPrefix}-${db}.sql.bz2"
        mysqldump -u${dbUser} -p${dbPass} --skip-extended-insert --force ${db} \
            | bzip2 --compress --force -9 > ${bzFile}
        sayif 2 "  Writing %F{blue}${bzPrefix}-${db}.sql.bz2.sha1"
        sha1sum ${bzFile} > "${bzFile}.sha1"
    }
} # }}}
bzExpiry () { # {{{
    typeset -a rmList
    typeset rmPrefix=$(printf '????-??-??-%s-*.sql*' ${HOST})
    sayif 1 "Purging backup files older than %F{cyan}${bzExpire}%F{blue} days."
    rmList=($(find ${bzPath} -type f -name ${rmPrefix} -mtime +${bzExpire}))
    [[ -z ${rmList} ]] && sayif 2 "  No files purged."
    for xFile (${rmList[@]}) {
        if [[ ${xFile} =~ 'grants' ]] {
            sayif 2 "  Purging %F{magenta}${xFile:t}"
        } else {
            sayif 2 "  Purging %F{blue}${xFile:t}"
        }
        ${cmdPurge} ${xFile}
    }
} # }}}
main() { #{{{
    typeset CONFIG oEXPIRE oUSER HELP=false
    # zParseOpts # {{{
    zparseopts -E -D -AOPTS -- h -help v+=VB -verbose: q+=QT -quiet: \
        c: -config: e: -expire: g -grants u: -user:
    [[ $? = 0 ]] || die "Error ${status}!"
    for o (${(k)OPTS}) {
        case ${o} in
            -c | --config)          CONFIG=${OPTS[$o]};;
            -e | --expire)          oEXPIRE=${OPTS[$o]};;
            -g | --grants)          dbGrants=true;;
            -h | --help)            HELP=true;;
            -u | --user)            oUSER=${OPTS[$o]};;
            -q)                     let VERBOSE-=${#QT};;
                 --quiet)           let VERBOSE-=${OPTS[$o]};;
            -v)                     let VERBOSE+=${#VB};;
                 --verbose)         let VERBOSE+=${OPTS[$o]};;
        esac
    }
    [[ ${1} == '--' ]] &&  shift;
    dbNames+=($@)
    # }}}
    # defaults & usage # {{{
    : ${CONFIG:='~/.config/mysql-backup.cf'}
    : ${dbUser:=$(id -un)}
    : ${dbSkips:='mysql|information_schema|performance_schema'}
    : ${dbGrants:=false}
    : ${bzPath:="${HOME}/mysql-backups"}
    : ${bzPrefix:="$(date "+%F")-${HOST}"}
    : ${bzExpire:=0}
    [[ -r ${~CONFIG} ]] && source ${~CONFIG}
    [[ -n ${oEXPIRE} ]] && bzExpire=${oEXPIRE}
    [[ -n ${oUSER} ]] && dbUser=${oUSER}
    cmdPurge=$(whence trash) || cmdPurge=$(whence rm)
    ${HELP} && usage
    [[ -r ${~CONFIG} ]] || die "Can't read %F{magenta}${CONFIG}"
    # }}}
    # Preamble # {{{
    sayif 1 "MySQL Backup…"
    sayif 2 "  Backup: %F{blue}${bzPath}"
    sayif 2 "  Config: %F{blue}${CONFIG}"
    sayif 2 "  Purge: %F{blue}${cmdPurge}"
    sayif 2 "  Verbosity: %F{blue}${VERBOSE}"
    # }}}
    umask 0077
    [[ -d ${bzPath} ]] || mkdir -m0700 ${bzPath}
    mysql -u${dbUser} -p${dbPass} -e'exit' 2> /dev/null
    [[ $? = 0 ]] || die "Cannot access MySQL as %F{y}${dbUser}"
    ${dbGrants} && bkGrants
    bkDatabases
    [[ (${bzExpire} > 0) ]] && bzExpiry
} #}}}
main $@
