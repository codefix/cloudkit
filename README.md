# [Codefix Cloudkit](https://bitbucket.org/codefix/cloudkit) #

This is a collection of scripts developed by and for Codefix Consulting Inc., clients, friends, and anyone who cares to make use of the code herein.

## brimstone ##

Brimstone helps automate cloud deployments. It is named after the resurrectionist in Laini Taylor's “Daughter of Smoke and Bone” trilogy.

## bootstrap ##

A stage one bootstrap script for Google Compute Cloud. Its primary function is to install *git* and *zsh*, then install a custom *bootstrap.sh* downloaded from a URL specified by an instance attribute named *bootstrap-url*. The second stage bootstrap is usually run by a user and sets up requirements for running **brimstone**.

## check-services ##

This script uses *service(8)* to query System V init scripts or upstart jobs, optionally restarting services where needed. The script will exit TRUE if all services are found running, otherwise the exit value will be equivalent to the number of configured services not running or restarted.

## gits ##

Run git commands on multiple working copies.

## ventus ##

Ventus is a wrapper intended to automate Google object storage operations using *.ventus* config files. Support for Amazon S3 is likely. In Latin, *ventus* means “wind”, ergo this script may be thought of as *a vento flante ad nubes*, “a wind that moves the clouds.”
